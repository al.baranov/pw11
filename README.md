# [Проектная работа 11: Continuous Integration](https://lms.skillfactory.ru/courses/course-v1:SkillFactory+DEVOPS-3.0+2021/courseware/1818f1e4a2fa4e2aace76827bb5ed460/a1e12cbce1184ec98f95980bc3a7f0cc/1?activate_block_id=block-v1%3ASkillFactory%2BDEVOPS-3.0%2B2021%2Btype%40vertical%2Bblock%40202de5c9a375428688f2e1d4f779bf2a)

Новые фичи в наше приложение добавляются несколько раз в неделю, но на их тестирование уходит много времени. Давайте уменьшим срок поставки изменений в продукт, внедрив подход непрерывной интеграции.

Мы уже изучили Docker, предлагаем теперь настроить деплой приложения в контейнеры с последующим тестированием обновления.

### TODO:
- [x] Создать в Я.Облаке виртуальную машину со следующими характеристиками: 2vCPU, 2GB, RAM, 20GB, HDD.
- [x] Поднять на этой машине CI-сервер на ваш выбор.
- [x] Создать репозиторий (github/gitlab/проч. на ваше усмотрение) и создать там файл index.html.
- [x] Настроить CI:
    - [x] Запускающий контейнер с nginx (версия на ваше усмотрение) с пробросом порта 80 в порт 9889 хостовой системы. При обращении к nginx в контейнере по HTTP, nginx должен выдавать измененный файл index.html.
    - [x] Проверяющий код ответа запущенного контейнера при HTTP-запросе (код должен быть 200).
    - [x] Сравнивающий md5-сумму измененного файла с md5-суммой файла, отдаваемого nginx при HTTP-запросе (суммы должны совпадать).
    - [x] Триггер для старта CI: внесение изменений в созданный вами файл index.html из п.3. В случае выявления ошибки (в двух предыдущих пунктах), должно отправляться оповещение вам в удобный канал связи — Telegram/Slack/email. Текст оповещения — на ваше усмотрение.
    - [x] После выполнения CI созданный контейнер удаляется.
- [x] Прислать ментору написанный вами для CI код (или скрин с описанием джоба), а также ссылку на репозиторий и на развернутую CI-систему.

- [x] Проверено, оценка 5/5

## Ответ:
* pipelines: <https://gitlab.com/roman-devops33/pw11/-/pipelines>
* [Screenshots](./screenshots/README.md)

___

* git tags: [link](https://git-scm.com/book/ru/v2/%D0%9E%D1%81%D0%BD%D0%BE%D0%B2%D1%8B-Git-%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0-%D1%81-%D1%82%D0%B5%D0%B3%D0%B0%D0%BC%D0%B8)
* docker nginx: <https://hub.docker.com/_/nginx>
* gitlab ci rules: <https://docs.gitlab.com/ee/ci/jobs/job_control.html#rules-examples>
* gitlab sudo: <https://stackoverflow.com/questions/19383887/how-to-use-sudo-in-build-script-for-gitlab-ci>
* gitlab runner ansible install: <https://github.com/riemers/ansible-gitlab-runner/blob/master/tasks/install-debian.yml>
* ansible get_url: <https://docs.ansible.com/ansible/latest/collections/ansible/builtin/get_url_module.html>
* ansible tips: <https://docs.ansible.com/ansible/latest/tips_tricks/ansible_tips_tricks.html>
* curl page status: <https://g-soft.info/notes/2482/curl-poluchit-kod-statusa-stranitsy-http-code/>
* pipline email status: <https://docs.gitlab.com/ee/user/project/integrations/pipeline_status_emails.html>
