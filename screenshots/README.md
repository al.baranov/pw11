### Запуски pipelines
![img](pipelines.png)
### Commit with pipline
![img](commit.png)
### Запуск Nginx
![img](build.png)
### Получение страницы
![img](get.png)
### Проверка контрольной суммы
![img](sum.png)
### Остановка контейнера 
![img](done.png)
### Оповещение о неудаче на email
![img](notification.png)
