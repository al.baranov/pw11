variable "cloud_id" {
  description = "Yandex cloud id"
  type        = string
}

variable "cloudflare_api_token" {
  description = "cloudflare token"
  type        = string
}

variable "folder_id" {
  description = "Devops-33 folder id"
  type        = string
}

variable "numpw" {
  description = "Number of project work"
  type        = string
}

variable "yakey" {
  description = "yandex service account key file path"
  type        = string
}

variable "zone" {
  description = "Default zone for resurces"
  type        = string
}

variable "cloudflare_zone_id" {
  description = "zone id of cloudflare"
  type        = string
}

variable "num" {
  description = "zone id of cloudflare"
  type        = number
  default     = 1
}