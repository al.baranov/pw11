# Токены лежат во внешнем файле, инициализацию проводить через команду: terraform init -backend-config=${TF_BACKEND_YC}
# Проверка: pre-commit run -a
terraform {
  required_providers {
    # Настройки провайдера яндекс облака
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.83.0"
    }
    # Настройки провайдера cloudflare
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
  }
  backend "s3" {
    endpoint                    = "storage.yandexcloud.net"
    bucket                      = "tf-bucket-for-state"
    region                      = "ru-central1-a"
    key                         = "state1/b11pw.tfstate"
    skip_region_validation      = true
    skip_credentials_validation = true
  }
  required_version = ">= 0.13"
}
# Настройки авторизации
provider "yandex" {
  service_account_key_file = file(var.yakey)
  cloud_id                 = var.cloud_id
  folder_id                = var.folder_id
  zone                     = var.zone
}

provider "cloudflare" {
  api_token = var.cloudflare_api_token
}