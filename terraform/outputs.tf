# Внешний ip виртуалки
# output "external_ip_address_vm" {
# value = yandex_compute_instance.vm.network_interface[0].nat_ip_address
# } old

output "external_ip_address_vm" {
  value = [for ip in yandex_compute_instance.vm : ip.network_interface[0].nat_ip_address]
}
