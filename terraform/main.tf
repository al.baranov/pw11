# Create VM
resource "yandex_compute_instance" "vm" {
  count       = var.num
  name        = "${var.numpw}-vm${count.index + 1}"
  platform_id = "standard-v3"
  zone        = var.zone

  resources {
    core_fraction = 20
    cores         = 2
    memory        = 2
  }

  # Прерываемый
  scheduling_policy {
    preemptible = true
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.image.id
      type     = "network-hdd"
      size     = 50
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet.id
    nat       = true
  }

  metadata = {
    user-data = file("C:/Users/User/ya-cloud-init.yml")
  }
}

# Получение image id, для получения списка образов: yc compute image list --folder-id standard-images
data "yandex_compute_image" "image" {
  family = "ubuntu-2004-lts"
}

# Создание сети
resource "yandex_vpc_network" "network" {
  name = "${var.numpw}-network"
}

# Подсеть
resource "yandex_vpc_subnet" "subnet" {
  name           = "${var.numpw}-subnet-1"
  v4_cidr_blocks = ["192.168.10.0/24"]
  zone           = var.zone
  network_id     = yandex_vpc_network.network.id
}

resource "cloudflare_record" "ssh" {
  count   = var.num
  zone_id = var.cloudflare_zone_id
  name    = "ssh${count.index + 1}"
  value   = yandex_compute_instance.vm[count.index].network_interface[0].nat_ip_address
  type    = "A"
  proxied = false
}

resource "cloudflare_record" "vm" {
  count   = var.num
  zone_id = var.cloudflare_zone_id
  name    = "vm${count.index + 1}"
  value   = yandex_compute_instance.vm[count.index].network_interface[0].nat_ip_address
  type    = "A"
  proxied = true
}
